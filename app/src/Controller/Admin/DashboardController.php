<?php

namespace App\Controller\Admin;

use App\Entity\Answer;
use App\Entity\Category;
use App\Entity\Force;
use App\Entity\Language;
use App\Entity\Question;
use App\Entity\QuestionAnswer;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends AbstractDashboardController
{
    /**
     * @Route("/admin", name="admin")
     */
    public function index(): Response
    {
        // redirect to some CRUD controller
        $routeBuilder = $this->get(AdminUrlGenerator::class);

        return $this->redirect($routeBuilder->setController(LanguageCrudController::class)->generateUrl());
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('Admin');
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::linktoDashboard('Dashboard', 'fa fa-home');
        yield MenuItem::linkToCrud('Answers', 'fas fa-question-circle', Answer::class);
        yield MenuItem::linkToCrud('Categories', 'fas fa-dragon', Category::class);
        yield MenuItem::linkToCrud('Forces', 'fas fa-jedi', Force::class);
        yield MenuItem::linkToCrud('Languages', 'fas fa-language', Language::class);
        yield MenuItem::linkToCrud('Questions', 'fas fa-question', Question::class);
        yield MenuItem::linkToCrud('Questions/Réponse', 'fas fa-question', QuestionAnswer::class);
    }
}
