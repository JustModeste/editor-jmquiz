<?php

namespace App\Controller\Admin;

use App\Entity\Force;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class ForceCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Force::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id')->hideOnForm(),
            AssociationField::new('language')->setRequired(true),
            TextField::new('label')
        ];
    }
}
